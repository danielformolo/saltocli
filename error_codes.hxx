#ifndef _ERROR_CODES_HXX_
#define _ERROR_CODES_HXX_

// code status to internal erros/returns.
//
#define RET_OK              0
#define ERR_FILE            10
#define ERR_PARSE           11
#define ERR_SYS             20
#define ERR_UNKNOWN         50

#define ERR_INVALID_CMD     -11



// user messages
//
#define USER_MSG_OK              "OK"
#define USER_MSG_ERR_COMMNAD     "ERR: command"
#define USER_MSG_ERR_LOADCFG     "ERR: load configuration"



// function codes
//
#define FUNC_DEV_SET            5000
#define FUNC_DEV_GET            5001
#define FUNC_DEV_DELETE         5002

#endif