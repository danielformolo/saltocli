## Folder Structure

1. Build: compile the project inside this folder. See docs/report.docx for more details.
2. Cli, Services, SaltoCLI.cpp, commands.cfg error_codes.hxx are the source files.
3. gtest and other internal gtest folders are the unit test source codes.
4. docs: contain the explanation about the project.

## See docs/report.docx for details about the software and how to compile and run it. 
