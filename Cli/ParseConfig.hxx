#ifndef _PARSECONFIG_HXX_
#define _PARSECONFIG_HXX_


#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>

#include "../error_codes.hxx"

using namespace std;


//
/// \class ParseConfig
/// \brief Read system configuration from disk and store them in datastructures.
/// \note  for now the only configuration is the list of acceptable commands.
//
class ParseConfig {
    ifstream ifs;
    string filename;
    map<string, int> command_list;

    public:

    ParseConfig(string _filename = "commands.cfg") {
        filename = _filename;
    }

    ~ParseConfig() {}


    /// \brief load configurations from file and keep them in datastructures
    int Load() {
        try {  //file.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
            ifs.open (filename);
            string line;
            while (getline(ifs, line)) {
                if (line[0] == '#') // comments starts with # at begining of a line. ignore them. 
                    continue;

                istringstream iss(line);
                int func_code;
                string user_label;
                if (!(iss >> func_code >> user_label)) { 
                    throw ERR_PARSE; 
                }
                string tmp;
                iss >> tmp;
                while (iss) {
                    user_label += " " + tmp;
                    iss >> tmp;
                }

                // add to the list of commands
                command_list[user_label] = func_code;
            }
            ifs.close();
        }
        catch (int e) { /// \todo add to syslog these events
            if (e == ERR_PARSE) {
                return ERR_PARSE;
            }
            return ERR_UNKNOWN;
        }
        catch (ifstream::failure e) {
            return ERR_FILE;
        }

        return RET_OK;
    }

    //
    /// \brief return the function code if  the command is valid, otherwise return -1;
    //
    int is_commmand_valid( string user_cmd) {
        auto it = command_list.find( user_cmd);
        if (it == command_list.end()) 
            return ERR_INVALID_CMD;

        return it->second;
    }

};



#endif