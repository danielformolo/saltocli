#include "../Cli.hxx"
#include <gtest/gtest.h>


TEST( Cli, Creation_and_Execution) {
    
    Cli cli;
    ASSERT_EQ(cli.Init(), RET_OK);
    ASSERT_EQ(cli.Run(1, "DELETE DeviceName"), "OK");
    ASSERT_EQ(cli.Run(2, "DELETE DeviceName"), "OK");
    ASSERT_EQ(cli.Run(3, "DELETE DeviceName"), "OK");
    ASSERT_EQ(cli.Run(1001, "DELETE DeviceName"), "OK");


    Cli * cli1 = new Cli();
    ASSERT_EQ(cli1->Init(), RET_OK);
    ASSERT_EQ(cli1->Run(1, "DELETE DeviceName"), "OK");
    delete cli1;

    Cli cli2;
    ASSERT_EQ(cli2.Run(1, "DELETE DeviceName"), "ERR: load configuration");

    ASSERT_EQ(cli.Run(1, "DELETEadsfasfasfasfasfasfa DevisadfasdfasdfasfdaceName asdfa fas fas"), "ERR: command");

}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}