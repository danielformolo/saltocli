#ifndef _CLI_HXX_
#define _CLI_HXX_


#include "ParseConfig.hxx"
#include "../Services/Device/ServiceDevices.hxx"

using namespace std;

//
/// \class Cli
/// \brief Manage the user interface, receive and process user commands
//
class Cli : private ParseConfig {
    ServiceDevices * service_devices;

    public:

    Cli() : ParseConfig() {service_devices = NULL;}

    Cli(string filename) : ParseConfig(filename) { service_devices = NULL;}

    ~Cli() {
        delete service_devices;
    }

    //
    /// \brief it initializes internal structures. it might be called before Run. 
    //
    int Init() {
        int ret = Load();
        if (ret != RET_OK) {
            cout << endl << USER_MSG_ERR_LOADCFG << " " << ret << endl;
            return ret;
        }
 
        service_devices = new ServiceDevices();
      
        return RET_OK;
    }

    //
    /// \brief it waits and process user commands.
    /// \param loop if 0 runs forever, for any other value it runs once.
    //
    string Run( int loop = 0, string cmd = "") {
        if (service_devices == NULL) // Object not initialized Init() not called yet.
            return USER_MSG_ERR_LOADCFG;

        int count_loops = 1;

        string ret;
        while (count_loops) {
            ret = USER_MSG_ERR_COMMNAD;
            if (loop != 0) count_loops--;

            string user_cmd;
            if (cmd == "") getline (cin, user_cmd);
            else user_cmd = cmd;

            istringstream iss(user_cmd);
            string param;
            string cmd1;
            string cmd2;
            if (!(iss >> cmd1 >> cmd2)) { 
                cout << USER_MSG_ERR_COMMNAD <<  endl;
                continue;
            }

            int func_code = is_commmand_valid( string(cmd1+" "+cmd2));
            if (func_code == ERR_INVALID_CMD) {
                cout << USER_MSG_ERR_COMMNAD <<  endl;
                continue;
            }

            if (func_code == FUNC_DEV_SET) {
                iss >> param;
                service_devices->set(param);
                if (service_devices->set( param) == RET_OK)
                    ret = USER_MSG_OK;
                else
                    ret = USER_MSG_ERR_COMMNAD;
            }
            else if (func_code == FUNC_DEV_GET) {
                string name;
                if (service_devices->get( name) == RET_OK)
                    ret =  name;
                else
                    ret = "";
            }
            else if (func_code == FUNC_DEV_DELETE) {
                if (service_devices->del() == RET_OK)
                    ret = USER_MSG_OK;
                else
                    ret = USER_MSG_ERR_COMMNAD;
            }

            cout << ret << endl;
        }

        return ret;
    }

};

#endif