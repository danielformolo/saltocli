#include "SaltoCLITest.cpp"
#include <gtest/gtest.h>


TEST( IntegrationTest, RegularCommands) { 
    // 2 ways to test, opted for the cleanest code alternative. 

    // this:
    //testing::internal::CaptureStdout();
    //thread_cli("SET DeviceName AAA");
    //std::string output = testing::internal::GetCapturedStdout(); 
    //ASSERT_EQ( "OK\n", output);
    
    // or this:
    ASSERT_EQ( thread_cli( "SET DeviceName AAA"), "OK");


    ASSERT_EQ( thread_cli( "SET DeviceName BBB"), "OK");
    ASSERT_EQ( thread_cli( "GET DeviceName"), "BBB");
    ASSERT_EQ( thread_cli( "SET DeviceName BBB"), "OK");
    ASSERT_EQ( thread_cli( "GET DeviceName"), "BBB");
    ASSERT_EQ( thread_cli( "DELETE DeviceName"), "OK");
    ASSERT_EQ( thread_cli( "GET DeviceName"), "");

    ASSERT_EQ( thread_cli( "DELETE DeviceName"), "OK");
    ASSERT_EQ( thread_cli( "DELETE DeviceName"), "OK");
    ASSERT_EQ( thread_cli( "DELETE DeviceName"), "OK");
    ASSERT_EQ( thread_cli( "DELETE DeviceName"), "OK");
    ASSERT_EQ( thread_cli( "DELETE DeviceName"), "OK");
    
    ASSERT_EQ( thread_cli( "SET DeviceName CCC"), "OK");
    ASSERT_EQ( thread_cli( "GET DeviceName"), "CCC");
    ASSERT_EQ( thread_cli( "DELETE DeviceName"), "OK");
    ASSERT_EQ( thread_cli( "DELETE DeviceName"), "OK");
    ASSERT_EQ( thread_cli( "SET DeviceName  "), "OK");   
    ASSERT_EQ( thread_cli( "GET DeviceName"), "");
 
    ASSERT_EQ( thread_cli( "SET DeviceName CCC"), "OK");
    ASSERT_EQ( thread_cli( "GET DeviceName"), "CCC");

    ASSERT_EQ( thread_cli( "SET DeviceName BBB dfasfasasfasf"), "OK");
    ASSERT_EQ( thread_cli( "GET DeviceName adfafda as sdf fa df"), "BBB");
    ASSERT_EQ( thread_cli( "SET DeviceName BBB"), "OK");
    ASSERT_EQ( thread_cli( "DELETE DeviceName asdf af af asfaf"), "OK");
    ASSERT_EQ( thread_cli( "GET DeviceName"), "");
 
    ASSERT_EQ( thread_cli( "SET     DeviceName     BBB"), "OK");
    ASSERT_EQ( thread_cli( "    GET       DeviceName adfafda as sdf fa df"), "BBB");
    ASSERT_EQ( thread_cli( "  SET DeviceName       BBB"), "OK");
    ASSERT_EQ( thread_cli( " DELETE    DeviceName asdf af af asfaf"), "OK");
    ASSERT_EQ( thread_cli( "GET DeviceName        "), "");
 
    //ASSERT_EQ(18.0, squareRoot(324.0));
    //ASSERT_EQ(25.4, squareRoot(645.16));
    //ASSERT_EQ(0, squareRoot(0.0));
}
 
TEST( IntegrationTest, Stress) {
    //testing::internal::CaptureStdout();
    for (int i = 0; i<10000; i++) { 
        ASSERT_EQ( thread_cli( "SET DeviceName AAA"), "OK");
        ASSERT_EQ( thread_cli( "SET DeviceName BBB"), "OK");
        ASSERT_EQ( thread_cli( "GET DeviceName"), "BBB");
        ASSERT_EQ( thread_cli( "SET DeviceName BBB"), "OK");
        ASSERT_EQ( thread_cli( "GET DeviceName"), "BBB");
        ASSERT_EQ( thread_cli( "DELETE DeviceName"), "OK");
        ASSERT_EQ( thread_cli( "GET DeviceName"), "");
        ASSERT_EQ( thread_cli( "SET DeviceName CCC"), "OK");
        ASSERT_EQ( thread_cli( "GET DeviceName"), "CCC");
        ASSERT_EQ( thread_cli( "SET DeviceName  "), "OK");
        ASSERT_EQ( thread_cli( "GET DeviceName"), "");
    }
}

TEST( IntegrationTest, WrongParameters) {
        ASSERT_EQ( thread_cli( "SETxx DeviceName AAA"), "ERR: command");
        ASSERT_EQ( thread_cli( "SET DeviceNameXX BBB"), "ERR: command");
        ASSERT_EQ( thread_cli( "SE DeviceNameXX BBB"), "ERR: command");
        ASSERT_EQ( thread_cli( "SET DeviceNam BBB"), "ERR: command");
        ASSERT_EQ( thread_cli( "BBB"), "ERR: command");
        
        ASSERT_EQ( thread_cli( "GET DeviceNam"), "ERR: command");
        ASSERT_EQ( thread_cli( "GE DeviceNam"), "ERR: command");
        ASSERT_EQ( thread_cli( "GETxx DeviceNam"), "ERR: command");
        ASSERT_EQ( thread_cli( "GET DeviceNamXX"), "ERR: command");

        ASSERT_EQ( thread_cli( "DEdsfLETE DeviceName"), "ERR: command");
        ASSERT_EQ( thread_cli( "DELETExxx DeviceName"), "ERR: command");
        ASSERT_EQ( thread_cli( "DELETE xxxDeviceName"), "ERR: command");
        ASSERT_EQ( thread_cli( "DELETE x DeviceName"), "ERR: command");
 
}
int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}