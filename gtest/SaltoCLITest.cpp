﻿#include <iostream>
#include <vector>
#include <string>
#include <chrono>
#include <sstream>
#include <thread> 
#include <pthread.h>

#include "../Cli/Cli.hxx"

using namespace std;

string thread_cli( string cmd) {
    Cli cli;

    if (cli.Init() == RET_OK || true)  { 
        return cli.Run( 1, cmd);
    }
    
    return "";
}
