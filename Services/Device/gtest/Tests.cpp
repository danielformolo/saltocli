#include "../ServiceDevices.hxx"
#include <gtest/gtest.h>


TEST( ServicesDevice, Creation) {
    ServiceDevices service_devices1;
    ASSERT_EQ(service_devices1.set("AAA"), RET_OK);

    ServiceDevices service_devices2("testfile2.cfg");
    ASSERT_EQ(service_devices2.set("AAA"), RET_OK);
   
    ServiceDevices * service_devices3 = new ServiceDevices("testfile3.cfg");
    ASSERT_EQ(service_devices3->set("AAA"), RET_OK);
    delete service_devices3;

    ServiceDevices service_devices4("./WrongDirectory/testfile.cfg");
    string s;
    service_devices4.set("444");
    ServiceDevices service_devices5;
    service_devices5.get(s);
    ASSERT_NE(s, "");
}
 
TEST( ServicesDevice, Commands) {
    ServiceDevices service_devices;
    string s;
    ASSERT_EQ(service_devices.set("AAA"), RET_OK);
    ASSERT_EQ(service_devices.del(), RET_OK);
    service_devices.get(s);
    ASSERT_EQ(s, "");

    ASSERT_EQ(service_devices.del(), RET_OK);
    ASSERT_EQ(service_devices.del(), RET_OK);
    ASSERT_EQ(service_devices.del(), RET_OK);

    ASSERT_EQ(service_devices.set("AAA"), RET_OK);
    ASSERT_EQ(service_devices.set("AAA"), RET_OK);
    ASSERT_EQ(service_devices.set("AAA"), RET_OK);
    service_devices.get(s);
    ASSERT_EQ(s, "AAA");

    ASSERT_EQ(service_devices.set("AA1"), RET_OK);
    ASSERT_EQ(service_devices.set("AA2"), RET_OK);
    ASSERT_EQ(service_devices.set("AA3"), RET_OK);
    service_devices.get(s);
    ASSERT_EQ(s, "AA3");

    ServiceDevices service_devices2;
    service_devices2.get(s);
    ASSERT_EQ(s, "AA3");

    ASSERT_EQ(service_devices.set("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaaaaaaaaa"), RET_OK);
    service_devices.get(s);
    ASSERT_EQ(s, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaaaaaaaaa");
   service_devices.get(s);
    ASSERT_EQ(s, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaaaaaaaaa");
   service_devices.get(s);
    ASSERT_EQ(s, "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaaaaaaaaa");
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}