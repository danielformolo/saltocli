#ifndef _SERVICECLIENTS_HXX_
#define _SERVICECLIENTS_HXX_

#include <iostream>
#include <fstream>
#include <string>

#include "../../error_codes.hxx"

using namespace std;

//
/// \class ServiceDevices
/// \brief It Treats services offered for the system. for now the only service is about the device name. 
/// \brief Other services not related to Device are treated in other service modules.
//
class ServiceDevices {
    string device_name;
    string filename;

    public:
    
    //
    /// \brief initialize this module and its internal state
    //
    ServiceDevices(string _filename = "devices.cfg") {
        filename = _filename;
        
        ifstream ifs;
        try {    
            ifs.open (filename);
            string line;
            getline(ifs, line);
            if (line.size() > 0) 
                device_name = line;
            ifs.close();
        }
        catch (ifstream::failure e) {/// \todo add to syslog these event
            return;
        }
    }

    ~ServiceDevices() { };

    //
    /// \brief set a new name to the device
    //
    int set(string name = "") {
        ofstream ofs;
        try {
            ofs.open ( filename, ofstream::out);
            ofs << name << endl;
            ofs.close();
        }
        catch (ofstream::failure e) {
            return ERR_FILE;
        }

        if (name == "") device_name.clear();
        else device_name = name;
        
        return RET_OK;
    }


    //
    /// \brief get the name of the device
    //
    int get( string& name) {
        if (device_name.empty())
            return ERR_INVALID_CMD;

        name = device_name;
        return RET_OK;
    }


    //
    /// \brief delete the name of the device
    //
    int del() {
        int ret = set();
        if (ret = RET_OK)
            device_name.empty();
        
        return ret;
    }

};

#endif