﻿#include <iostream>
#include <vector>
#include <string>
#include <chrono>
#include <sstream>
#include <thread> 
#include <pthread.h>

#include "Cli/Cli.hxx"

using namespace std;

void thread_cli() {
    Cli cli;

    if (cli.Init() == RET_OK) { 
        cli.Run();
    }
}


int main(int argc, char * argv[]) {
    
    string sarg;
    if (argc == 2) {
        sarg = argv[1];
    }
    // let main process free to do other things in paralel or to start new services
    // and keep Client Line Interface responsive to users.
    //thread runcli(thread_cli, sarg);
    thread runcli(thread_cli);
    runcli.join(); 

    return 0;
}
